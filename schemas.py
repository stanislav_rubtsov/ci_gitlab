from pydantic import BaseModel, Field


class ShortRecording(BaseModel):
    title: str = Field(
        ..., title="The name of the recipe", min_length=3, max_length=100
    )
    number_views: int = Field(..., title="The number of recipe views", ge=1)
    cooking_time: int = Field(..., title="Cooking time", ge=1)


class ShortRecordingOut(ShortRecording):
    id: int

    class Config:
        from_attributes = True


class Recipe(BaseModel):
    title: str = Field(
        ..., title="The name of the recipe", min_length=3, max_length=100
    )
    cooking_time: int = Field(..., title="Cooking time", ge=1)
    ingredients: str = Field(
        ...,
        title="List of ingredients",
        min_length=3,
    )
    description: str = Field(..., title="Recipe description")


class RecipeIn(Recipe): ...


class RecipeOut(Recipe):
    id: int

    class Config:
        from_attributes = True
