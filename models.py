from sqlalchemy import Column, ForeignKey, Integer, String

from database import Base


class ShortRecording(Base):
    __tablename__ = "short_recordings"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    number_views = Column(Integer, default=0)
    cooking_time = Column(Integer, nullable=False)


class Recipe(Base):
    __tablename__ = "recipes"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    cooking_time = Column(Integer, nullable=False)
    ingredients = Column(String, nullable=False)
    description = Column(String)
    recording_id = Column(
        Integer, ForeignKey("short_recordings.id", ondelete="CASCADE")
    )
