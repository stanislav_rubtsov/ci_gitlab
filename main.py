from typing import Optional, Sequence

from fastapi import FastAPI, Request
from fastapi.exceptions import ResponseValidationError
from fastapi.responses import JSONResponse
from sqlalchemy.future import select

import models
import schemas
from database import engine, session

app = FastAPI()


@app.exception_handler(ResponseValidationError)
async def validation_exception_handler(
    request: Request, exc: ResponseValidationError
) -> JSONResponse:
    """Обработка исключений для передачи данных по ошибке клиенту"""
    return JSONResponse(content={"detail": exc.errors()}, status_code=400)


@app.on_event("startup")
async def startup() -> None:
    """Подготовка приложения к работе"""
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown() -> None:
    """Завершение работы приложения"""
    await session.close()
    await engine.dispose()


@app.post("/recipes/", response_model=schemas.RecipeOut)
async def recipes_post(recipe: schemas.RecipeIn) -> models.Recipe:
    """Внесение в БД нового рецепта"""
    new_recipe: models.Recipe = models.Recipe(**recipe.dict())
    async with session.begin():
        query = select(models.Recipe).filter(
            models.Recipe.title == new_recipe.title
        )
        res = await session.execute(query)
        result: Optional[models.Recipe] = res.scalars().one_or_none()
        if result:
            raise ResponseValidationError(
                f"Рецепт {new_recipe.title} уже внесен в базу"
            )

        new_short_recording: models.ShortRecording = models.ShortRecording(
            title=new_recipe.title,
            number_views=1,
            cooking_time=new_recipe.cooking_time,
        )
        session.add(new_short_recording)
        await session.flush()

        new_recipe.recording_id = new_short_recording.id
        session.add(new_recipe)
        await session.commit()

    return new_recipe


@app.get("/recipes/short", response_model=list[schemas.ShortRecordingOut])
async def recipes_get() -> Sequence[models.ShortRecording]:
    """Вывод списка всех рецептов (короткая версия рецептов)"""
    async with session.begin():
        res = await session.execute(
            select(models.ShortRecording).order_by(
                models.ShortRecording.number_views.desc(),
                models.ShortRecording.cooking_time,
            )
        )
    return res.scalars().all()


@app.get("/recipes", response_model=list[schemas.RecipeOut])
async def recipes() -> Sequence[models.Recipe]:
    """Вывод списка всех рецептов (полная версия рецептов)"""
    async with session.begin():
        res = await session.execute(select(models.Recipe))
    return res.scalars().all()


@app.get("/recipes/{idx}", response_model=schemas.RecipeOut)
async def recipes_idx(idx: int) -> models.Recipe:
    """Вывод рецепта по номеру записи в БД"""
    async with session.begin():
        result: Optional[models.Recipe] = await session.get(models.Recipe, idx)
        if not result:
            raise ResponseValidationError(
                f"Запись под номером {idx} в БД отсутствует"
            )
        id_short_record: int | None = result.recording_id
        short_record: models.ShortRecording | None = await session.get(
            models.ShortRecording, id_short_record
        )
        if not short_record:
            raise ResponseValidationError(
                f"Запись под номером {id_short_record} в БД отсутствует"
            )
        else:
            if not short_record.number_views:
                short_record.number_views = 1
            else:
                short_record.number_views = short_record.number_views + 1
        await session.commit()
    return result


@app.get("/")
async def main():
    return {"msg": "Cookbook"}
