from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Cookbook"}


def test_recipes_post():
    test_data = {
        "title": "Test",
        "cooking_time": 10,
        "ingredients": "string",
        "description": "string",
    }
    response = client.post("/recipes/", json=test_data)
    assert response.status_code == 200
    assert response.json()["title"] == "Test"


def test_recipes_post_err():
    test_data = {
        "title": "Test",
        "cooking_time": 10,
        "ingredients": "string",
        "description": "string",
    }
    response = client.post("/recipes/", json=test_data)
    assert response.status_code == 400
    assert response.json() == {"detail": "Рецепт Test уже внесен в базу"}


def test_recipes_short():
    response = client.get("/recipes/short")
    assert response.status_code == 200
    assert response.json()[0]["title"] == "Test"


def test_recipes_get():
    response = client.get("/recipes")
    assert response.status_code == 200
    assert response.json()[0]["title"] == "Test"


def test_recipes_id():
    response = client.get("/recipes/1")
    assert response.status_code == 200
    assert response.json()["title"] == "Test"


def test_recipes_short_count():
    response = client.get("/recipes/short")
    assert response.status_code == 200
    assert response.json()[0]["number_views"] > 1
